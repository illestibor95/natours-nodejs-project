const express = require('express');
const bookingController = require('../controllers/bookingController');
const authController = require('../controllers/authController');
const factory = require('../controllers/handlerFactory');
//merge params
//default each router has access to its own params [tourId]
const router = express.Router();

router.get(
  '/checkout-session/:tourId',
  authController.protect,
  bookingController.getCheckoutSession
);
router.use(
  authController.protect,
  authController.restrictTo('admin', 'lead-guide')
);

router
  .route('/')
  .get(bookingController.getAllBooking)
  .post(bookingController.deleteBooking);

router
  .route('/:id')
  .get(bookingController.getBooking)
  .patch(bookingController.updateBooking)
  .delete(bookingController.createBooking);

module.exports = router;
