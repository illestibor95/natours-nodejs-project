/* eslint-disable*/
export const displayMap = (locations) => {
  mapboxgl.accessToken =
    'pk.eyJ1IjoiaWxsZXN0aWJvcjk1IiwiYSI6ImNrYXc5cWlxajAyOXkycm12bmF1bWpidmMifQ.nTFjJ5-4W3cfIeYwZVom1g';
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/illestibor95/ckaw9xlgl0ku11irurjcpdv3z',
    center: [-118.113491, 34.111745],
    scrollZoom: false,
  });

  const bounds = new mapboxgl.LngLatBounds();

  locations.forEach((loc) => {
    // Create marker
    const el = document.createElement('div');
    el.className = 'marker';

    //Add marker
    new mapboxgl.Marker({
      element: el,
      anchor: 'bottom',
    })
      .setLngLat(loc.coordinates)
      .addTo(map);

    // Add popup
    new mapboxgl.Popup({
      offset: 30,
    })
      .setLngLat(loc.coordinates)
      .setHTML(`<p>Day ${loc.day}: ${loc.description}</p>`)
      .addTo(map);

    // extend the map bounds to include current location
    bounds.extend(loc.coordinates);
  });

  map.fitBounds(bounds, {
    padding: { top: 200, bottom: 150, left: 100, right: 100 },
  });
};
